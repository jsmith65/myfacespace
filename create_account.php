<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Welcome to MyFaceSpace!">
	<meta name="author" content="Jon Scott Smith and Jordan Smith">

	<title>Create</title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="css/signin.css" rel="stylesheet">
</head>

<body>
	<?php
	session_start();

	if (isset($_POST['name'])) // POST
	{
		// To use functions that upload and resize images
		require("image_util.php");   

		// Connect and select database
		$username = "jsmith2";
		$password = "H00155858";
		$database = "jsmith2";
		$mysqli = new mysqli('localhost', $username, $password, $database);

		// Get submitted data and verify it wasn't left blank
		$name = trim($mysqli->real_escape_string($_POST['name']));
		$username = trim($mysqli->real_escape_string($_POST['username']));
		$password = trim($mysqli->real_escape_string($_POST['password']));

		if ($name == '')
			ShowError("Please go back and enter your name.");

		// Don't allow short usernames or usernames with non-alphanumeric characters
		if (strlen($username) < 3 || !ctype_alnum($username))
			ShowError("Please go back and enter a username that is at least 3 characters and only 
				composed of letters and numbers.");

		if ($password == '')
			ShowError("Please go back and enter a password.");


		$upload_dir = 'myfacespace/images';
		$upload_url = "/~jsmith2/$upload_dir";

		// This is the directory the uploaded images will be placed in.
		// It must have priviledges sufficient for the web server to write to it
		$upload_directory_full = "/home/jsmith2/public_html/$upload_dir";
		if (!is_writeable($upload_directory_full))
			  ShowError("The directory $upload_directory_full is not writeable.\n");

		$image_filename = "$upload_directory_full/$username.jpg";

		// Save the uploaded image to the given filename
		$error_msg = UploadSingleImage($image_filename);
		if ($error_msg != "")
			ShowError($error_msg);

		// Save uploaded image with a maximum width or height of 300 pixels
		CreateThumbnailImage($image_filename, $image_filename, 300);

		// Create a small thumbnail of the image to be used later
		$image_thumbnail = $username . "_thumb.jpg";
		CreateThumbnailImage($image_filename, "$upload_directory_full/$image_thumbnail", 60);

		// Get the MD5 hash of the password for inserting into the database
		$password_hash = md5($password);

		// Insert record into the database
		$sql = "INSERT INTO Users VALUES ('$username', '$password_hash', '$name', '', NULL)";
		if ($mysqli->query($sql))
		{
			// Set session variable for use in other pages
			$_SESSION['login'] = 'yes';
			$_SESSION['username'] = $_POST['username'];
			$_SESSION['incorrect'] = false;
			header("Location: index.php");
		}
		elseif ($mysqli->errno == 1062)
			ShowError("Sorry, but the username <b>$username</b> already exists. Please go back and 
				choose another.");
		else
			ShowError("Error ($mysqli->errno) $mysqli->error<br>SQL = $sql\n");
	} // end POST
	else
	{
		// Display the form
		require("header.inc");
	?>

	<div class="container">

		<form class="form-signin" role="form" method="POST" action="create_account.php" enctype="multipart/form-data">
			<input type="hidden" name="MAX_FILE_SIZE" value="5000000">
			<h2 class="form-signin-heading">Create an account</h2>
			<input type="text" name="name" class="form-control" placeholder="Name" required autofocus>
			<input type="text" name="username" class="form-control" placeholder="Username" required>
			<input type="password" name="password" class="form-control" placeholder="Password" required>
			<label for="profilePic">Profile Image</label>
			<input type="file" name="imgfile" id="profilePic" required>
			<p class="help-block">Image must be a jpeg file.</p>
			<!--
			<label class="checkbox">
				<input type="checkbox" value="remember-me"> Remember me
			</label>
			-->
			<button class="btn btn-lg btn-primary btn-block" type="submit">Create</button>
		</form>

	</div> <!-- /container -->

	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<?php

	} // end else

	?>
</body>
</html>

<?php
function ShowError($error)
{
?>
  <h1>Unable to create account</h1>
  <p><?= $error ?></p>
	  <p><a href="javascript:history.back()">Go back</a></p>
</body>
</html>
<?php
  exit;
}
?>