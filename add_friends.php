<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Your profile page.">
		<meta name="author" content="Jon Scott Smith and Jordan Smith">
		<!--<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">-->

		<title>Profile</title>

		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.css" rel="stylesheet">
	</head>

	<body>
		<div class="container">
			<!-- Example row of columns
			<div class="row"> -->
		<?php
			session_start();

			require("header.inc");
			require("database.inc");
			require("time.inc");

			$user = trim($mysqli->real_escape_string($_SESSION['username']));

			$sql = "SELECT username1 FROM Friends WHERE username2 = '$user' UNION 
	 				SELECT username2 FROM Friends WHERE username1 = '$user';";

			$result = $mysqli->query($sql) or
				die("Error executing query: ($mysqli->errno) $mysqli->error<br>SQL = $sql");

			$friends = array();

			$row = $result->fetch_row();

			while($row)
			{
				$friends[$row[0]] = true;
				$row = $result->fetch_row();
			}

			if(isset($_GET['query']))
			{
				$query = trim($mysqli->real_escape_string($_GET['query']));

				$sql = "SELECT * FROM Users WHERE name LIKE '%$query%' AND username != '$user' ORDER BY name;";

				$result = $mysqli->query($sql) or
					die("Error executing query: ($mysqli->errno) $mysqli->error<br>SQL = $sql");

				$row = $result->fetch_row();

				while($row)
				{
					echo "<div class='row'><div class='col-md-12' style='padding: 15px; border-bottom: solid grey;'>";
					echo "<img src='images/$row[0].jpg' class='img-thumbnail img-responsive' alt='Profile Picture of $row[2]'>";
					if(isset($friends[$row[0]]))
					{
						echo "<h2><a href='?user=$row[0]'>$row[2]</a></h2>";
					}
					else
					{
						echo "<h2>$row[2]</h2>";
					}
					if($row[3])
					{
						echo "<div style='line-height: 20px;'>$row[3]<br />";
						echo "<font style='color: grey; font-size: small;'>";
						timeSincePosted($row[4]);
						echo "</font>";
					}
					if(!isset($friends[$row[0]]))
					{
						echo "<br><a href='add_friend_db.php?user=$row[0]' class='btn btn-default'>Add as Friend</a>";
					}
					if($row[3])
					{
						echo "</div>";
					}
					echo "</div></div>";
					
					$row = $result->fetch_row();

				}
			}

		?>

			
		</div>
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>