<?php
session_start();

// If the login session var is not set to "yes" then
// redirect the browser back to the login screen
if (!isset($_SESSION['login']) || $_SESSION['login'] != 'yes')
	header("Location: login.php");
else
{
	if($_POST['status'])
	{
		require("database.inc");

		$status = trim($mysqli->real_escape_string($_POST['status']));
		$username = trim($mysqli->real_escape_string($_SESSION['username']));

		$sql = "UPDATE Users SET status = '$status', updatetime = NOW() WHERE username = '$username';";

		$result = $mysqli->query($sql) or
			die("Error executing query: ($mysqli->errno) $mysqli->error<br>SQL = $sql");
	}
	header("Location: index.php");
}
?>