<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Welcome to MyFaceSpace!">
	<meta name="author" content="Jon Scott Smith and Jordan Smith">

	<title>Edit</title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="css/signin.css" rel="stylesheet">
</head>

<body>
	<?php
	session_start();

	require("header.inc");
	// Profile image locations
	$upload_dir = 'myfacespace/images';
	$upload_url = "/~jsmith2/$upload_dir";
		
	// Connect and select database
	$username = "jsmith2";
	$password = "H00155858";
	$database = "jsmith2";
	$mysqli = new mysqli('localhost', $username, $password, $database);

	if ($_SERVER['REQUEST_METHOD'] == 'GET')
	{	
		// Get user data from database
			
		$username = $_SESSION['username'];
		
		$sql = "SELECT * FROM Users WHERE username='$username'";
		$result = $mysqli->query($sql) or
			Error("Error executing query: ($mysqli->errno) $mysqli->error<br>SQL = $sql");

		$row = $result->fetch_assoc();
		if (!$row)
			Error("Unable to find the record for user $username.");
		
		$name = $row['name'];
		$profile_img = "$upload_url/$username.jpg";
		
	?>

	<div class="container">

		<form class="form-signin" role="form" method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data">
			<input type="hidden" name="MAX_FILE_SIZE" value="5000000">
			<h2 class="form-signin-heading">Edit your account</h2>
			<input type="text" name="name" class="form-control" placeholder="New Name" value="<?= $name ?>">
			<input type="password" name="password" class="form-control" placeholder="New Password">
			<label for="imgfile">Profile Image</label>
			<input type="file" name="imgfile" id="imgfile">
			<p class="help-block">Image must be a jpeg file.</p>
			<!--
			<label class="checkbox">
				<input type="checkbox" value="remember-me"> Remember me
			</label>
			-->
			<button class="btn btn-lg btn-primary btn-block" type="submit">Save</button>
		</form>

	</div> <!-- /container -->

	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
<?php

}
else   // POST
{	
	require('image_util.php');   // To use functions that upload and resize images
	
	if (!isset($_POST['name']))
		Error("Name could not be found. This could be due to the image submitted being too
			large. Go back and select an image that is no larger than " . ini_get('upload_max_filesize')
			. ".");	
	
	// Get submitted data and verify it wasn't left blank
	$name = trim($mysqli->real_escape_string($_POST['name']));
	$username = $_SESSION['username'];
	$password = trim($_POST['password']);
		
	if ($name == '')
		Error("Go back and enter your name.\n");

	if (ImageUploaded())
	{		
		// This is the directory the uploaded images will be placed in.
		// It must have priviledges sufficient for the web server to write to it
		$upload_directory = "/home/jsmith2/public_html/$upload_dir";
		if (!is_writeable($upload_directory)) 
			Error("The directory $upload_directory is not writeable.\n");


		$image_filename = "$upload_directory/$username.jpg";
		
		// Save the uploaded image to the given filename
		$error_msg = UploadSingleImage($image_filename);
		if ($error_msg != "")
		{
			Error($error_msg);
		}	
		
		// Save uploaded image with a maximum width or height of 300 pixels
		CreateThumbnailImage($image_filename, $image_filename, 300);
		
		// Create a very small thumbnail of the image to be used later
		$image_thumbnail = $username . "_thumb.jpg";
		CreateThumbnailImage($image_filename, "$upload_directory/$image_thumbnail", 60);
	}
	
	// Get the MD5 hash of the password for inserting into the database
	if ($password != "")
		$password_hash = md5($password);
	
	// Insert record into the database
	$sql = "UPDATE Users SET name='$name' ";
	if ($password != "")
	{
		$password_hash = md5($password);
		$sql .= ", password='$password_hash'";
	}
		
	$sql .= " WHERE username='$username'";
	
	$mysqli->query($sql) or
		Error("Error executing query: ($mysqli->errno) $mysqli->error<br>SQL = $sql");
		
	$_SESSION['name'] = $name;

	header('Location: index.php');
	
}  // end POST


function Error($error)
{
?>
    <h1>Unable to edit the account</h1>
    <p><?= $error ?></p>
	<p><a href="javascript:history.back()">Go back</a></p>
</body>
</html>
<?php
    exit;
} 
?> 
