<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Welcome to MyFaceSpace!">
	<meta name="author" content="Jon Scott Smith and Jordan Smith">

	<title>Login</title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="css/signin.css" rel="stylesheet">
</head>

<body>
	<?php
	session_start();

	if (isset($_POST['password']))
	{
		require("database.inc");

		// Password was posted so see if this password
		// is the user's password

		// Get the user's password from the Users table
		$sql = "SELECT username, password FROM Users WHERE " .
				"username='$_POST[username]'";
		 
		// Issue the query and output any error messages incurred
		$result = $mysqli->query($sql) or
			die("Error executing query: ($mysqli->errno) $mysqli->error<br>SQL = $sql");
		 
		// Loop through all the rows returned by the query
		$row = $result->fetch_row();
		
		// Get hash for the submitted password
		$password_hash = md5($_POST['password']);
			
		if ($password_hash == $row[1])
		{
			//echo "You successfully logged in.";
			
			// Set a session variable that indicates the user is
			// logged in
			$_SESSION['login'] = 'yes';
			$_SESSION['username'] = $_POST['username'];
			$_SESSION['incorrect'] = false;
			
			// Redirect to index.php by sending to the browser
			// a 302 redirect
			header("Location: index.php");
		}
		else
		{
			$_SESSION['incorrect'] = true;
			header("Location: login.php");
		}
	}
	else
	{
		// Display the form
		require("header.inc");
		if(isset($_SESSION['incorrect']))
		{
			if($_SESSION['incorrect'])
			{
				echo "<div class='alert alert-danger'><strong>Oops!</strong> Username or password incorrect.</div>";
			}
		}
	?>

	<div class="container">

		<form class="form-signin" role="form" method="POST" action="login.php">
			<h2 class="form-signin-heading">Please sign in</h2>
			<input type="text" name="username" class="form-control" placeholder="Username" required autofocus>
			<input type="password" name="password" class="form-control" placeholder="Password" required>
			<!--
			<label class="checkbox">
				<input type="checkbox" value="remember-me"> Remember me
			</label>
			-->
			<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
		</form>

	</div> <!-- /container -->

	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <?php

	} // end else

    ?>
</body>
</html>
