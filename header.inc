<div class="navbar navbar-inverse navbar-static-top" style="margin-bottom: 0px;" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">MyFaceSpace</a>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<?php
				if (!isset($_SESSION['login']) || $_SESSION['login'] != 'yes')
				{
					echo '<li><a href="login.php">Login</a></li>
						  <li><a href="create_account.php">Create Account</a></li>';
				?>
			</ul>
				<?php
				}
				else
				{
				?>

				<li><a href="index.php">Home</a></li>
				<li><a href="edit_account.php">Edit</a></li>
				<li><a href="logout.php">Logout</a></li>
			</ul>

			<div class="col-sm-3 col-md-3 pull-right">
				<form class="navbar-form pull-right" role="search" method="GET" action="add_friends.php">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search for Friends" name="query" id="srch-term">
						<div class="input-group-btn">
							<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
						</div>
					</div>
				</form>
			</div>
			<?php

			} //end else

			?>
		</div><!--/.nav-collapse -->
	</div>
</div>