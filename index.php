<!-- taz.harding.edu/~jsmith2/myfacespace -->
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Your profile page.">
		<meta name="author" content="Jon Scott Smith and Jordan Smith">
		<!--<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">-->

		<title>Profile</title>

		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.css" rel="stylesheet">

		<script>
		function confirmRemove(e)
		{
			var person = e.target.id.split("-");
			if(confirm('Are you sure you want to unfriend ' + person[1] + '?'))
			{
				window.location.replace(location.href.replace("index.php", "remove_friend_db.php?user=") + person[0]);
			}
		}
		</script>
	</head>

	<body>

		<?php //Block 1
			session_start();

			// If the login session var is not set to "yes" then
			// redirect the browser back to the login screen
			if (!isset($_SESSION['login']) || $_SESSION['login'] != 'yes')
				header("Location: login.php");
			else
			{
				require("header.inc");
				require("database.inc");
				require("time.inc");

				?>

				<!-- Main jumbotron for users profile -->
				<div class="jumbotron">
					<div class="container">

				<?php
				if(isset($_GET['user'])) //If viewing friend's page
				{
					$user = $_GET['user'];

					$sql = "SELECT username, name, status, updatetime FROM Users WHERE username = '$user';";

					$result = $mysqli->query($sql) or
						die("Error executing query: ($mysqli->errno) $mysqli->error<br>SQL = $sql");

					$row = $result->fetch_row();

					echo "<img src='images/$row[0].jpg' alt='Profile Picture of $row[1]' class='img-rounded'
							style='float: left; margin:0 15px 0 0;'>";
					echo "<h1 style='margin-top: 0px;'>$row[1]</h1>";

					if($row[2])
					{
						echo "<div style='line-height: 20px;'>$row[2]<br />";
						echo "<font style='color: grey; font-size: small;'>";
						timeSincePosted($row[3]);
						echo "</font></div>";
					}
				?>
					</div>
				</div>

				<div class="container">
					<!-- Example row of columns -->
					<div class="row">

				<?php
					$sql = "SELECT * FROM 
						 (SELECT u.username, u.name, u.status, u.updatetime FROM Users u, Friends f WHERE 
						 f.username1='$user' AND f.username2 = u.username UNION 
						 SELECT u.username, u.name, u.status, u.updatetime FROM Users u, Friends f WHERE 
						 f.username2='$user' AND f.username1 = u.username) temp ORDER BY updatetime DESC;";

					$friends = $mysqli->query($sql) or
						die("Error executing query: ($mysqli->errno) $mysqli->error<br>SQL = $sql");


					$friend = $friends->fetch_row();

					while($friend)
					{
						echo "<div class='col-md-12' style='padding: 15px; border-bottom: solid grey;'>";
						echo "<img src='images/$friend[0].jpg' class='img-thumbnail img-responsive' alt='Profile Picture of $friend[1]'>";
						echo "<h2>$friend[1]</h2>";
						if($friend[2])
						{
							echo "<div style='line-height: 20px;'>$friend[2]<br />";
							echo "<font style='color: grey; font-size: small;'>";
							timeSincePosted($friend[3]);
							echo "</font></div>";
						}
						echo "</div>";
						
						$friend = $friends->fetch_row();
					}
				}
				else //If viewing profile page.
				{
					$sql = "SELECT username, name, status, updatetime FROM Users WHERE username = '$_SESSION[username]';";

					$result = $mysqli->query($sql) or
						die("Error executing query: ($mysqli->errno) $mysqli->error<br>SQL = $sql");

					$row = $result->fetch_row();

					echo "<img src='images/$row[0].jpg' alt='Profile Picture of $row[1]' class='img-rounded'
							style='float: left; margin:0 15px 0 0;'>";
					echo "<h1 style='margin-top: 0px;'>$row[1]</h1>";

					if($row[2])
					{
						echo "<div style='line-height: 20px;'>$row[2]<br />";
						echo "<font style='color: grey; font-size: small;'>";
						timeSincePosted($row[3]);
						echo "</font></div>";
					}
			?> <!-- Block 1 End -->

						<form class="form-inline" role="form" method="POST" action="set_status.php">
							<div class="form-group">
								<input type="text" name='status' class="form-control" placeholder="What's on your mind?" required>
							</div>
							<button type="submit" class="btn btn-default">Share</button>
						</form>

					</div>
				</div>

				<div class="container">
					<!-- Example row of columns -->
					<div class="row">

					<?php //Block 2
					$sql = "SELECT * FROM 
						 (SELECT u.username, u.name, u.status, u.updatetime FROM Users u, Friends f WHERE 
						 f.username1='$_SESSION[username]' AND f.username2 = u.username UNION 
						 SELECT u.username, u.name, u.status, u.updatetime FROM Users u, Friends f WHERE 
						 f.username2='$_SESSION[username]' AND f.username1 = u.username) temp ORDER BY updatetime DESC;";

					$friends = $mysqli->query($sql) or
						die("Error executing query: ($mysqli->errno) $mysqli->error<br>SQL = $sql");


					$friend = $friends->fetch_row();

					if($friend == NULL)
					{
						echo "<div class='alert alert-info'><strong>Friendless?</strong> Search above to find some!</div>";
					}
					else
					{
						while($friend)
						{
							echo "<div class='col-md-12' style='padding: 15px; border-bottom: solid grey;'>";
							echo "<img src='images/$friend[0].jpg' class='img-thumbnail img-responsive' alt='Profile Picture of $friend[1]'>";
							echo "<form class='form-inline' role='form'><div class='form-group'><a href='index.php?user=$friend[0]'><h2>$friend[1]</h2></a>
									<div class='form-group'>
									<button class='btn btn-default' type='button' id='$friend[0]-$friend[1]'><i class='glyphicon glyphicon-remove'></i></button></form>
									</div></div>
									<script>
										var button = document.getElementById('$friend[0]-$friend[1]');
										button.addEventListener('click', confirmRemove, false);
									</script>";
							if($friend[2])
							{
								echo "<div style='line-height: 20px;'>$friend[2]<br />";
								echo "<font style='color: grey; font-size: small;'>";
								timeSincePosted($friend[3]);
								echo "</font></div>";
							}
							echo "</div>";
							
							$friend = $friends->fetch_row();
						}
					}
				}
			}
					?> <!-- Block 2 End -->
			</div>
		</div> <!-- /container -->

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>