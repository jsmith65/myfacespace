<?php
function timeSincePosted($timePosted)
{
	$currentTime = new DateTime(null, new DateTimeZone('America/Chicago'));
	$statusTime = new DateTime($timePosted, new DateTimeZone('America/Chicago'));
	$diff = $statusTime->diff($currentTime);
	$seconds = $diff->format('%s');
	$minutes = $diff->format('%i');
	$hours = $diff->format('%h');
	$days = $diff->format('%d');
	$months = $diff->format('%m');

	if($months != 0)
	{
		echo "$months months ago";
	}
	elseif($days != 0)
	{
		echo "$days days ago";
	}
	elseif($hours != 0)
	{
		echo "$hours hours ago";
	}
	elseif($minutes != 0)
	{
		echo "$minutes minutes ago";
	}
	else
	{
		echo "$seconds seconds ago";
	}
}
?>